#!/bin/sh
zfs_mount()
{
echo Mount zfs disk
zfs set readonly=off llroot
zfs mount -a
adjkerntz -i
/etc/rc.d/syscons start
}


mountro=`zfs get -H readonly llroot/ROOT/master | cut -w -f 3`
[ ${mountro} = "on" ] && zfs_mount
usrsrc=${USRSRC:-/usr/src}

etcupdate -p && \
make -C ${usrsrc} installworld && \
etcupdate && \
make -C ${usrsrc} delete-old && \
echo "reboot to delete old libs"
# make -C ${usrsrc} -DBATCH_DELETE_OLD_FILES delete-old
# make -C ${usrsrc} -DBATCH_DELETE_OLD_FILES delete-old-libs

