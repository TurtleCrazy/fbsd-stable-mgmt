#!/bin/sh

usage() {
	echo 'Usage: ...'
	echo '-c	clean obj'
	echo '-u	svn update'
	echo '-j	jobs to be handled'
	echo 'default to ' ${jarg}
	echo '---------------------'
}

parse() {
	RED='\033[0;31m'
	NC='\033[0m' # No Color
	while getopts "cnj:" option
	do
		case ${option} in
			c)
				CLEANING=1
				;;
			n)
				UPDATE=1
				;;
			j)
				case ${OPTARG} in
				(*[!0-9]*|'')
					echo -e "${RED}number expected for jobs${NC}; default value used."
					;;
				(*)
					jarg=${OPTARG}
					;;
				esac
				;;
		esac
	done
}


jarg=`sysctl -n hw.ncpu`
usrsrc=${USRSRC:-/usr/src}

parse ${*}

if [ ${UPDATE:=-0} -eq 0 ]; then
	echo "updating ..."
	git -C ${usrsrc} pull --rebase
	[ ${?} == 1 ] && exit 1
fi


if [ ${CLEANING:=-0} -eq 1 ]; then
	echo Cleaning
	chflags -R noschg /usr/obj/* && sync && rm -rf /usr/obj && sync
fi

echo building process on ${jarg} jobs
make -j${jarg} -C ${usrsrc} buildworld && make -j${jarg} -C ${usrsrc} kernel \
	&& echo "Reboot the system."


