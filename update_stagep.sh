#!/bin/sh

usage() {
	echo 'Usage: ...'
	echo '-u	svn update'
	echo '-j	jobs to be handled'
	echo 'default to ' ${jarg}
	echo '---------------------'
}

parse() {
	RED='\033[0;31m'
	NC='\033[0m' # No Color
	while getopts "uj:" option 
	do
		case ${option} in
			u)
				UPDATE=1
				;;
			j)
				case ${OPTARG} in 
				(*[!0-9]*|'') 
					echo -e "${RED}number expected for jobs${NC}; default value used."
					;;
				(*) 
					jarg=${OPTARG} 
					;;
				esac
				;;
		esac
	done
}


jarg=`sysctl -n hw.ncpu`


if [ $? -ne 0 ] 
then 
	usage 
else
	parse ${*} 
fi

if [ ${UPDATE:=-0} -eq 1 ]; then
	echo "Updating port Tree"
	poudriere ports -p HEAD -u 
fi

echo ------------------------------------------------------------------
echo                         building lapinbilly
echo ..................................................................

poudriere bulk -J ${jarg}:2   -j owncloud -p HEAD -f /usr/local/etc/poudriere.d/lp-list

echo
echo ------------------------------------------------------------------
echo                         building llanura
echo ..................................................................

poudriere bulk -J ${jarg}:2   -j release11 -p HEAD -f /usr/local/etc/poudriere.d/llanura-list
