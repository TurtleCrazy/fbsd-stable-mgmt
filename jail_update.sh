#!/bin/sh

# Update freebsd base of all running jails
# 
# one has first to create the suitable configuration file 
# by default /usr/local/etc/jail-update.conf
# or pass it as the first argument
# 
#

usage() {
	echo 'Usage: ...'
	echo -e '-c\tconfiguration file'
	echo -e '\t\tdefault to /usr/local/etc/jail-update.conf'
	echo -e '-j\tjail list'
	echo -e '\t\tdefault to all running jails'
	echo -e '-u\tthis message'
	echo '-------------------------------------------------'
}

parse() {
	RED='\033[0;31m'
	NC='\033[0m' # No Color
	while getopts "cuj:" option 
	do
		case ${option} in
			c)
				CONF=${OPTARG}
				;;
			j)
				JLIST=${OPTARG}
				;;
			*)
				usage
				exit 0
				;;
		esac
	done
}

# default parameters
CONF=/usr/local/etc/jail-update.conf
# get currently running names (or jids) list
JLIST=`jls name 2>/dev/null`

parse ${*}

[ -z "${JLIST}" ] && echo 'no jail to check.exiting.' && exit 0
[ ! -r "${CONF}" ] && echo 'no configuration file for updating.exiting.' && exit 1

echo ''
echo 'Ready to update jail(s) <'${JLIST}'>, according to the <'${CONF}'> configuration.'
echo ''

for J in ${JLIST};
do
	# jail path
	JPATH=`jls -j ${J} path 2>/dev/null` 
	if [ -n "${JPATH}" ]; then
		echo '[ :: ' $J ' ::]'
		# freebsd version this jail is running
		JVERSION=`jexec ${J} freebsd-version -u`
		if [ -n "${JVERSION}" ]; then
			echo 'Updating <'${J}'> from version <'${JVERSION}'>, path <'${JPATH}'>.'
			freebsd-update -f  ${CONF} --currently-running ${JVERSION}  -b ${JPATH} fetch install 

		else
			echo 'Unable to get jail <'${J}'> path.Skipping'

		fi

	else
		echo 'Jail <'${J}'> unavailable.Skipping.'

	fi
done

