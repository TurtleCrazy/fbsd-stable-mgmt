#!/bin/sh

all_jails=`jls jid`
DO_CLEAN=""
DO_DIST=""

do_all_jails(){

	echo '---------------------------------------'
	echo ":: Host ::"
	echo
	pkg upgrade
	echo
	echo Cleaning host 
	echo
	[ -n "${DO_CLEAN}" ] && pkg ${DO_CLEAN}
	[ -n "${DO_DIST}" ] && pkg ${DO_DIST}
	echo '---------------------------------------'
	for j in ${all_jails}
	do
		echo
		echo '---------------------------------------'
		echo ":: jail `jls -j ${j} name` [ ${j} ] ::"
		echo ''

		pkg -j ${j} upgrade
		echo
		echo Cleaning 
		echo
		[ -n "${DO_CLEAN}" ] && pkg -j ${j} ${DO_CLEAN}
		echo
		echo Cleanup distfiles
		echo
		[ -n "${DO_DIST}" ] && pkg -j ${j} ${DO_DIST}
		echo '---------------------------------------'
	done
}

echo 
read -r -p "Would you like to clean up (autoremove) ports ? [Y/n] " yn
echo 

case "${yn}" in
	[Yy][eE][sS]|[yY])
		DO_CLEAN="autoremove"
		;;
	*) 
		;;
esac


echo 
read -r -p "Would you like to remove distfiles ? [Y/n] " yn
echo 

case "${yn}" in
	[Yy][eE][sS]|[yY])
		DO_DIST="clean -a"
		;;
	*) 
		;;
esac
do_all_jails 
